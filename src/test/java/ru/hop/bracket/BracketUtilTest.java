package ru.hop.bracket;

import org.junit.Test;

import static org.junit.Assert.*;

public class BracketUtilTest {

    @Test
    public void testFindAllbracketsWithOtherCharacters() {
        BracketUtil.findAllbrackets("{([qweqwasd])ouybyjitnciytvniyugtv iugv iutv ou(){}{(kjh ugouyf iutf iuy ((we123)asdada))}}").forEach(System.out::println);
    }

    @Test
    public void testFindAllBracketsCount() {
        assertSame(BracketUtil.findAllbrackets("{([])(){}{((()){})}}").size(), 10);
    }

    @Test
    public void testFindAllBracketsAllFieldsNotNull() {
        BracketUtil.findAllbrackets("{([])(){}{((()){})}}").forEach(bracket -> {
            assertNotNull(bracket.getType());
            assertTrue(bracket.getStartPosition() >= 0);
            assertTrue(bracket.getEndPosition() >= 0);
        });
    }

    @Test
    public void testFindAllBracketsWithNeighborBrackets() {
        BracketUtil.findAllbrackets("{([][])()()()()()[]{}{}{}{}{((()))}}").forEach(System.out::println);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAllBracketsWithExcessBracket() {
        BracketUtil.findAllbrackets("{([][]])()()()()()[]{}{}{}{}{((()))}}").forEach(System.out::println);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAllBracketsWithWrongOrder() {
        BracketUtil.findAllbrackets("{([][][)]()()()()()[]{}{}{}{}{((()))}}").forEach(System.out::println);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAllBracketsWithoutStartBracket() {
        BracketUtil.findAllbrackets("}))))]]})").forEach(System.out::println);
    }

}
