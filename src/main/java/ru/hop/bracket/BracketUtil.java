package ru.hop.bracket;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BracketUtil {
    public static List<Bracket> findAllbrackets(String input) {
        ArrayList<Bracket> brackets = new ArrayList<>();
        LinkedList<Bracket> stack = new LinkedList<>();
        for(int i=0; i < input.length(); i++) {
            char currentChar = input.charAt(i);
            BracketType bracketTypeStart = BracketType.getBracketTypeByStart(String.valueOf(currentChar));
            if (bracketTypeStart != null) {
                stack.push(new Bracket(i, -1, bracketTypeStart));
            } else {
                if (stack.isEmpty()) {
                    throw new IllegalArgumentException("Incorrect input string");
                }
                BracketType bracketTypeEnd = BracketType.getBracketTypeByEnd(String.valueOf(currentChar));
                if (bracketTypeEnd != null) {
                    Bracket bracket = stack.pop();
                    if (bracket.getType().equals(bracketTypeEnd)) {
                        bracket.setEndPosition(i);
                        brackets.add(bracket);
                    } else {
                        throw new IllegalArgumentException("Incorrect input string");
                    }
                }
            }
        }
        if (!stack.isEmpty()) {
            throw new IllegalArgumentException("Incorrect input string");
        }

        return brackets;
    }
}