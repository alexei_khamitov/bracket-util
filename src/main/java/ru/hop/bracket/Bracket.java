package ru.hop.bracket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Bracket {
    private int startPosition;
    private int endPosition;
    private BracketType type;
}
