package ru.hop.bracket;

public enum BracketType {
    ROUND("(", ")"),
    CURLY("{", "}"),
    SQUARE("[", "]");

    private String start;
    private String end;

    BracketType(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public static BracketType getBracketTypeByStart(String start) {
        for (BracketType bt : BracketType.values()) {
            if (bt.start.equals(start)) {
                return bt;
            }
        }
        return null;
    }
    public static BracketType getBracketTypeByEnd(String end) {
        for (BracketType bt : BracketType.values()) {
            if (bt.end.equals(end)) {
                return bt;
            }
        }
        return null;
    }
}